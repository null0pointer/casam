class Node:

	def __init__(self):
		raise Exception("Cannot initialize base Node class.")

	def evaluate(self, args):
		raise NotImplementedError()
	
	def derivative(self, partial):
		raise NotImplementedError()

	def simplified(self):
		return self.copy()

	def copy(self):
		raise NotImplementedError()

class Add(Node):

	def __init__(self, left, right):
		self.left = left
		self.right = right

	def evaluate(self, args):
		return self.left.evaluate(args) + self.right.evaluate(args)

	def derivative(self, partial):
		dLeft = self.left.derivative(partial)
		dRight = self.right.derivative(partial)
		return Add(dLeft, dRight).simplified()

	def simplified(self):
		sLeft = self.left.simplified()
		sRight = self.right.simplified()

		# Left and right are both constants to just add them and return
		if type(sLeft) is Constant and type(sRight) is Constant:
			return Constant(sLeft.evaluate({}) + sRight.evaluate({}))

		if type(sLeft) is Constant and sLeft.evaluate({}) is 0:
			return sRight

		if type(sRight) is Constant and sRight.evaluate({}) is 0:
			return sLeft

		return Add(sLeft, sRight)

	def copy(self):
		return Add(self.left.copy(), self.right.copy())

	def __str__(self):
		return "({} + {})".format(str(self.left), str(self.right))

class Mul(Node):

	def __init__(self, left, right):
		self.left = left
		self.right = right

	def evaluate(self, args):
		return self.left.evaluate(args) * self.right.evaluate(args)

	def derivative(self, partial):
		dLeft = self.left.derivative(partial)
		dRight = self.right.derivative(partial)
		return Add(Mul(self.left, dRight), Mul(dLeft, self.right)).simplified()

	def simplified(self):
		sLeft = self.left.simplified()
		sRight = self.right.simplified()

		# Left and right are both constants to just add them and return
		if type(sLeft) is Constant and type(sRight) is Constant:
			return Constant(sLeft.evaluate({}) * sRight.evaluate({}))

		if type(sLeft) is Constant:
			if sLeft.evaluate({}) is 0:
				return Constant(0)
			elif sLeft.evaluate({}) is 1:
				return sRight

		if type(sRight) is Constant:
			if sRight.evaluate({}) is 0:
				return Constant(0)
			elif sRight.evaluate({}) is 1:
				return sLeft

		return Mul(sLeft, sRight)

	def copy(self):
		return Mul(self.left.copy(), self.right.copy())

	def __str__(self):
		return "({} * {})".format(str(self.left), str(self.right))

class Exp(Node):

	def __init__(self, base, exp):
		self.base = base
		self.exp = exp

	def evaluate(self, args):
		return self.base.evaluate(args) ** self.exp.evaluate(args)

	def derivative(self, partial):
		exp = self.exp.simplified()
		dBase = self.base.derivative(partial)

		# Special Case: f^r for Real valued r
		if type(exp) is Constant:
			# (f^r)' = r * f' * f^(r-1)
			factor = Mul(dBase, Constant(exp.evaluate({})))
			exponent = Exp(self.base.copy(), Constant(exp.evaluate({}) - 1))
			return Mul(factor, exponent).simplified()

		# General Rule: (f^g)' = f^g * (f' * (g/f) + (g' * log(e, f)))
		dExp = self.exp.derivative(partial)

		# TODO: Finish this once I have log implemented


	def copy(self):
		return Exp(self.base.copy(), self.exp.copy())

	def __str__(self):
		return "{}^{}".format(str(self.base), str(self.exp))

class Constant(Node):

	def __init__(self, value):
		self.value = value

	def evaluate(self, args):
		return self.value
	
	def derivative(self, partial):
		return Constant(0)

	def copy(self):
		return Constant(self.value)		

	def __str__(self):
		return str(self.value)

class Variable(Node):

	def __init__(self, name):
		self.name = name

	def evaluate(self, args):
		if self.name in args.keys():
			return args[self.name]
		else:
			raise ValueError("Variable {} not defined.".format(self.name))
	
	def derivative(self, partial):
		if partial is self.name:
			return Constant(1)
		else:
			return Constant(0)

	def copy(self):
		return Variable(self.name)

	def __str__(self):
		return self.name

if __name__ == "__main__":
	x = Variable('x')
	p = Mul(Constant(3), x)
	f = Exp(Add(p, Constant(2)), Constant(2))
	print("f(x) = {}".format(str(f)))
	print("f(5) = {}".format(f.evaluate({'x': 5})))
	print("df/dx = {}".format(str(f.derivative('x'))))
